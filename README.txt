Workflow State buttons
==============

Simple module to add a state wise button to a node form.
Hard coded form_id for now. Could be dynamic in the near future.


Installations
==============

Installation is like any other module. Uncompress the .tar.gz file and drop it into the "sites/all/modules" subdirectory. Visit Administer >> Site building >> Modules, tick the box in front of the module name and press "Save configuration".

Configurations
==============

1. Admin >> structure >> content types >> "Edit your content type" >> Publishing Options >> Tick "Use workflow state wise buttons"
2. Workflow button lables configuration Admin >> Configuration >> Workflow >> Workflow State Buttons Configuration


