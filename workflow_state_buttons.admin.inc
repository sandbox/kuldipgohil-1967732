<?php

/**
 * @file
 * Functions that are only called on the admin pages.
 */

/**
 * Module settings form.
 */
function workflow_state_buttons_admin_settings() {
  $form['form_description'] = array(
    '#markup' => t("This form will allow you to set button label for each state of all workflows available in system."),
  );
  foreach (workflow_get_workflows() as $data) {
    $these_workflows[$data->wid] = check_plain(t($data->name));
     // Each workflow wise button label settings
      $form['workflow_state_buttons_' . $data->wid] = array(
        '#type' => 'fieldset',
        '#title' => check_plain(t($data->name)),
        '#description' => t('State wise button label settings for - ' . check_plain(t($data->name))),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
      );

      $states = workflow_get_workflow_states_by_wid($data->wid);
      foreach ($states as $state) {
        $form['workflow_state_buttons_' . $data->wid]['workflow_state_buttons_state_label_' . $state->sid] = array(
          '#type' => 'textfield',
          '#size' => 20,
          '#title' => $state->state,
          '#default_value' => variable_get('workflow_state_buttons_state_label_' . $state->sid, $state->state),
        );
    }
}

  return system_settings_form($form);
}
